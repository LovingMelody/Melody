//! Music Library
//! Designed specificaly for
//! the melody binary

#![deny(
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    const_err,
    deprecated,
    overflowing_literals,
    unreachable_patterns,
    unused_must_use,
    unused_mut,
    while_true
)]
#![warn(unreachable_pub, variant_size_differences)]
// TODO: Implement tag edit feature
// TODO: Stable Tag Detection (duration etc) currently iffy
// TODO: Write tests
// TODO: Write benchmarks
// TODO: Write docs

mod errors;
mod song;
/// Utilities for melody
pub mod utils;

mod musicplayer;
/// Collection of supported files (the file extension)
pub const SUPPORTED_FILETYPES: [&str; 3] = ["mp3", "ogg", "wav"];
pub use crate::errors::{MelodyErrors, MelodyErrorsKind};
pub use crate::musicplayer::{MinimalMusicPlayer, MusicPlayer, MusicPlayerStatus};
pub use crate::song::{Playlist, Song};
