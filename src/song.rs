use crate::utils::{fmt_duration, list_supported_files};
use std::convert::AsRef;
use std::fmt;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::time::Duration;

use crate::errors::MelodyErrors;
use tabwriter::TabWriter;

#[derive(Copy, Clone, Eq, PartialEq, Debug, PartialOrd, Ord)]
pub enum Status {
    Playing(::std::time::Instant, ::std::time::Duration),
    Stopped(::std::time::Duration),
}

impl Status {
    // Time elapsed
    pub fn elapsed(self) -> ::std::time::Duration {
        match self {
            Status::Stopped(d) => d,
            Status::Playing(start, extra) => start.elapsed() + extra,
        }
    }
    pub fn stop(&mut self) {
        *self = match *self {
            Status::Stopped(_) => *self,
            Status::Playing(start, extra) => Status::Stopped(start.elapsed() + extra),
        };
    }
    pub fn resume(&mut self) {
        *self = match *self {
            Status::Playing(_, _) => *self,
            Status::Stopped(duration) => Status::Playing(::std::time::Instant::now(), duration),
        };
    }
    pub fn is_stopped(self) -> bool {
        match self {
            Status::Stopped(_) => true,
            _ => false,
        }
    }
}

/// Song struct
#[derive(Clone, Eq, PartialEq, Debug, PartialOrd, Ord)]
pub struct Song {
    /// Artist of the song
    pub artist: Option<String>,
    /// Album of the song
    pub album: Option<String>,
    /// Title of the song
    pub title: Option<String>,
    /// Track of the song
    pub track: Option<u32>,
    /// Duration of the song
    pub duration: Duration,
    /// Genre of the song
    pub genre: Option<String>,
    /// File path to the song
    pub file: PathBuf,
    /// Elapsed time of song playing or Start time
    pub status: Status,
}

impl fmt::Display for Song {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let duration = fmt_duration(&self.duration);
        write!(
            f,
            "{} - {} - {} ({})",
            self.artist().unwrap_or("Unknown Artist"),
            self.album().unwrap_or("Unknown Album"),
            self.title().unwrap_or("Unknown Title"),
            duration
        )
    }
}

impl Song {
    /// Optionally return the artist of the song
    /// If `None` it wasn't able to read the tags
    pub fn artist(&self) -> Option<&str> {
        match self.artist.as_ref() {
            Some(artist) => Some(artist),
            None => None,
        }
    }
    /// Optionally return the song's album
    /// If `None` failed to read the tags
    pub fn album(&self) -> Option<&str> {
        match self.album.as_ref() {
            Some(album) => Some(album),
            None => None,
        }
    }
    /// Optionally return the title of the song
    /// If `None` it wasn't able to read the tags
    pub fn title(&self) -> Option<&str> {
        match self.title.as_ref() {
            Some(title) => Some(title),
            None => None,
        }
    }
    /// Optionally returns the song's track number
    /// If `None` it wasn't able to read the tags
    pub fn track(&self) -> Option<u32> {
        self.track
    }
    /// Optionally returns the song's genre
    /// If `None` it wasn't able to read the tags
    pub fn genre(&self) -> Option<&str> {
        match self.genre.as_ref() {
            Some(genre) => Some(genre),
            None => None,
        }
    }
    /// Returns the `Duration` of the song
    pub fn duration(&self) -> Duration {
        self.duration
    }
    /// Returns the elapsed time the song has been played
    pub fn elapsed(&self) -> Duration {
        self.status.elapsed()
    }
    /// Pause the song
    pub fn stop(&mut self) {
        self.status.stop()
    }
    /// Resume the song
    pub fn resume(&mut self) {
        self.status.resume()
    }
    /// Check if the song is stopped/paused
    pub fn is_stopped(&self) -> bool {
        self.status.is_stopped()
    }
    /// Returns the path of the song
    pub fn file(&self) -> &Path {
        &self.file
    }
    /// Load song from `Pathbuf`
    pub fn load(file: PathBuf) -> Result<Self, MelodyErrors> {
        let id3_tag = if let Ok(tag) = ::id3::Tag::read_from_path(&file) {
            tag
        } else {
            return Err(MelodyErrors {
                kind: crate::MelodyErrorsKind::FailedToReadTag,
                description: String::from("Failed to read meta data"),
                file: Some(file),
            });
        };

        let artist: Option<String> = id3_tag.artist().and_then(|s| Some(String::from(s)));
        let album: Option<String> = id3_tag.album().and_then(|s| Some(String::from(s)));
        let title: Option<String> = id3_tag.title().and_then(|s| Some(String::from(s)));
        let genre: Option<String> = id3_tag.genre().and_then(|s| Some(String::from(s)));
        let track: Option<u32> = id3_tag.track();
        let duration = ::mp3_duration::from_path(&file).map_err(|_| {
            MelodyErrors::new(
                crate::MelodyErrorsKind::MissingDuration,
                "Failed to get duration",
                Some(&file),
            )
        })?;

        Ok(Self {
            artist,
            album,
            title,
            genre,
            track,
            duration,
            file,
            status: Status::Stopped(::std::time::Duration::from_nanos(0)),
        })
    }
    /// Checks if the song is the same
    /// if matching_genre is true it will check genre as well
    pub fn matching_song(&self, s: &Song, matching_genre: bool) -> bool {
        if self.artist() != s.artist() {
            return false;
        }
        if self.album() != s.album() {
            return false;
        }
        if self.title() != s.title() {
            return false;
        }
        if self.track() != s.track() {
            return false;
        }
        if (self.genre() != s.genre()) && matching_genre {
            return false;
        }
        if self.track() != s.track() {
            return false;
        }
        if self.duration() == s.duration() {
            return false;
        }
        true
    }
    /// Checks if the song is an exact match
    /// Checks the song's tags and if the path is the same
    pub fn exact_match(&self, s: &Song, same_path: bool) -> bool {
        self.matching_song(s, true) && ((self.file() == s.file()) | same_path)
    }
}

impl AsRef<Path> for Song {
    fn as_ref(&self) -> &Path {
        &self.file
    }
}

/// Collection of Songs
#[derive(Debug)]
pub struct Playlist {
    /// List of songs in the playlist
    pub tracks: Vec<Song>,
}

impl Playlist {
    /// Create a playlist from a directory
    /// will walk through the directory and
    /// collect the songs it can process
    pub fn from_dir(path: PathBuf) -> Option<Self> {
        if !path.exists() {
            return None;
        };
        if path.is_file() {
            if let Ok(song) = Song::load(path) {
                return Some(Self { tracks: vec![song] });
            } else {
                return None;
            }
        };
        println!("Collecting tracks..");
        let mut tracks: Vec<Song> = list_supported_files(&path)
            .filter_map(|f| Song::load(f).ok())
            .collect();
        tracks.dedup();
        tracks.sort();
        Some(Self { tracks })
    }
    /// Returns if the playlist is currently empty
    pub fn is_empty(&self) -> bool {
        self.tracks.is_empty()
    }
    /// Takes `FnMut(Song)` and then maps & filters each song based on
    /// the functions result
    pub fn filter_map(&mut self, f: impl FnMut(Song) -> Option<Song>) {
        self.tracks = self.tracks.clone().into_iter().filter_map(f).collect();
    }
}

impl From<Vec<Song>> for Playlist {
    fn from(tracks: Vec<Song>) -> Self {
        Self { tracks }
    }
}

impl fmt::Display for Playlist {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut tw = TabWriter::new(Vec::new());
        let mut lines: Vec<String> = vec![String::from(
            "|\tArtist\t|\tAlbum\t|\tTitle\t|\tDuration\t|",
        )];
        for track in &self.tracks {
            lines.push(format!(
                "|\t{}\t|\t{}\t|\t{}\t|\t{}\t|",
                track.artist().unwrap_or("Unknown Artist"),
                track.album().unwrap_or("Unknown Album"),
                track.title().unwrap_or("Unknown Title"),
                fmt_duration(&track.duration)
            ))
        }
        write!(tw, "{}", lines.join("\n")).unwrap();
        tw.flush().unwrap();
        f.write_str(&String::from_utf8(tw.into_inner().unwrap()).unwrap())
    }
}

#[test]
fn test_pause() {
    let mut status = Status::Playing(
        ::std::time::Instant::now(),
        ::std::time::Duration::from_secs(0),
    );
    status.stop();
    let duration = match status {
        Status::Stopped(d) => d,
        _ => panic!("Unexpected value"),
    };
    ::std::thread::sleep(::std::time::Duration::from_secs(5));
    status.resume();
    dbg!(duration);
    assert_eq!(status.elapsed().as_secs(), duration.as_secs())
}
